package jenkins.jenkins.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HomeControllerTest {

    private HomeController testObj;

    @Test
    void shouldSumCorrectly() {
        // Given
        testObj = new HomeController();
        // When
        final int result = testObj.sum(2, 5);

        // Then
        Assert.assertEquals(7, result);
    }

}
